from .settings_file import settingsdict
from .soaprequests import createjobstring, createloginSOAP, closeJobString
import time
import pandas
import requests
import json
import datetime
import sqlite3
import io

my_settings = settingsdict

username = my_settings['username']
if my_settings['password']:
    password = my_settings['password'] + my_settings['token']
else:
    print("Password required!")
    raise SystemError("SF Password required")


def getXMLData(xmlstring, tagName):
    # XML Parser
    q = xmlstring.split('<' + tagName + '>')

    if len(q) > 1:
        return q[1].split('</' + tagName + '>')[0]
    else:
        return None


class sfSession:
    # Grabs the SessionId and ensure it's kept valid
    def __init__(self, username=username, password=password):
        self.username = username
        self.password = password
        self.sessiondetails = self.login(self.username, self.password)
        if self.sessiondetails:
            self.instance = self.sessiondetails['instance']
            self.sessionId = self.sessiondetails['sessionId']
            self.expirytime = datetime.datetime.now() + datetime.timedelta(
                seconds=int(self.sessiondetails['sessionSecondsValid']) - 60)
            # Take off 60 seconds to acount for computer time lag
        else:
            raise Exception("Unable to authenticate credentials")

    def login(self, username, password):
        # Returns a dictionary of metadata retrieved from authenticating with Salesforce
        request = createloginSOAP(username, password)
        session_details = {}
        encoded_request = request.encode('utf-8')
        url = "https://login.salesforce.com/services/Soap/u/43.0"

        headers = {"Content-Type": "text/xml; charset=UTF-8",
                   "SOAPAction": "login"}

        response = requests.post(url=url,
                                 headers=headers,
                                 data=encoded_request,
                                 verify=True)
        self.full_response = response.text
        if getXMLData(response.text, 'faultstring'):
            getXMLData(response.text, 'faultstring')
            return None

        session_details['sessionId'] = getXMLData(response.text, 'sessionId')
        session_details['passwordExpired'] = getXMLData(response.text, 'passwordExpired')
        session_details['sessionSecondsValid'] = getXMLData(response.text, 'sessionSecondsValid')

        serverURL = getXMLData(response.text, 'serverUrl')
        session_details['instance'] = serverURL[8:serverURL.find('.salesforce.com')]

        return session_details

    def isExpired(self):
        if self.expirytime > datetime.datetime.now():
            return False
        else:
            return True

    def refreshToken(self):
        self.sessiondetails = self.login(self.username, self.password)
        self.sessionId = self.sessiondetails['sessionID']
        self.expirytime = datetime.datetime.now() + datetime.timedelta(
            seconds=self.sessiondetails['sessionSecondsValid'] - 60)

    def getheaders(self, content_type="application/xml; charset=UTF-8"):

        if self.isExpired():
            self.refreshToken()

        headers = {"X-SFDC-Session": self.sessionId,
                   "Content-Type": content_type}
        return headers

    def __repr__(self):
        if self.sessionId:
            return str(self.sessionId)
        else:
            return 'Unable to authenticate with Salesforce. Check settings file'


class sfJob:
    def __init__(self, operation='', object='', session='', jobid=''):

        if not session:
            session = sfSession()

        self.session = session
        # Create new job
        if not jobid:
            postdata = createjobstring(operation=operation
                                                    , object=object
                                                    , concurrency="Parallel"
                                                    , contenttype="CSV")
            postdata = postdata.encode('utf-8')
            url = "https://" + session.instance + ".salesforce.com/services/async/43.0/job"

            self.operation = operation
            self.object = object
            self.batches = {}
            headers = self.session.getheaders()

            response = requests.post(url=url, headers=headers, data=postdata, verify=True)

            self.updateSelf(response.text)

        else:
            self.jobid = jobid
            self.updateSelf(self.getJobUpdate())
            self.operation = 'Unknown'
            self.object = 'Unknown'

    def updateSelf(self, xml_response_text):

        self.jobid = getXMLData(xml_response_text, 'id')
        self.createdby = getXMLData(xml_response_text, 'createdById')
        self.createdDT = getXMLData(xml_response_text, 'createdDate')
        self.numberBatchesQueued = getXMLData(xml_response_text, 'numberBatchesQueued')
        self.numberBatchesInProgress = getXMLData(xml_response_text, 'numberBatchesInProgress')
        self.numberBatchesCompleted = getXMLData(xml_response_text, 'numberBatchesCompleted')
        self.numberBatchesFailed = getXMLData(xml_response_text, 'numberBatchesFailed')
        self.numberBatchesTotal = getXMLData(xml_response_text, 'numberBatchesTotal')
        self.state = getXMLData(xml_response_text, 'state')
        self.numberRecordsProcessed = getXMLData(xml_response_text, 'numberRecordsProcessed')
        self.numberRetries = getXMLData(xml_response_text, 'numberRetries')
        self.operation = getXMLData(xml_response_text, 'operation')
        self.apiVersion = getXMLData(xml_response_text, 'apiVersion')
        self.numberRecordsFailed = getXMLData(xml_response_text, 'numberRecordsFailed')
        self.totalProcessingTime = getXMLData(xml_response_text, 'totalProcessingTime')
        self.apiActiveProcessingTime = getXMLData(xml_response_text, 'apiActiveProcessingTime')
        self.apexProcessingTime = getXMLData(xml_response_text, 'apexProcessingTime')
        self.jobInfo = getXMLData(xml_response_text, 'jobInfo')
        self.operation = getXMLData(xml_response_text, 'operation')
        self.object = getXMLData(xml_response_text, 'object')
        return None

    def addbatch(self, postdata):
        session = self.session
        url = "https://" + session.instance + ".salesforce.com/services/async/43.0/job/{jobid}/batch".format(
            jobid=self.jobid)
        headers = self.session.getheaders('text/csv; charset=UTF-8')
        response = requests.post(url=url,
                                 headers=headers,
                                 data=postdata,
                                 )
        batchId = getXMLData(response.text, 'id')
        self.unfinished_business = True
        self.batches[batchId] = {}
        self.batches[batchId]['jobID'] = getXMLData(response.text, 'jobId')
        self.batches[batchId]['state'] = getXMLData(response.text, 'state')
        self.batches[batchId]['numberRecordsProcessed'] = getXMLData(response.text, 'numberRecordsProcessed')
        self.batches[batchId]['numberRecordsFailed'] = getXMLData(response.text, 'numberRecordsFailed')

        return response.text

    def getJobUpdate(self):
        session = self.session
        url = "https://{instance}.salesforce.com/services/async/43.0/job/{jobid}".format(instance=session.instance,
                                                                                         jobid=self.jobid)

        headers = self.session.getheaders()

        response = requests.get(url=url, headers=headers)
        return response.text

    def getbatchinfo(self, batchid):
        session = self.session
        url = "https://{instance}.salesforce.com/services/async/43.0/job/{jobid}/batch/{batchid}"

        headers = self.session.getheaders()

        url = url.format(instance=session.instance, jobid=self.jobid, batchid=batchid)
        response = requests.get(url=url, headers=headers)

        return response.text

    def updatebatch(self):
        if self.batches:
            for key in self.batches:
                newdata = self.getbatchinfo(key)
                self.batches[key]['state'] = getXMLData(newdata, 'state')
                self.batches[key]['numberRecordsProcessed'] = getXMLData(newdata, 'numberRecordsProcessed')
                self.batches[key]['numberRecordsFailed'] = getXMLData(newdata, 'numberRecordsFailed')

                if getXMLData(newdata, 'stateMessage'):
                    self.batches[key]['stateMessage'] = getXMLData(newdata, 'stateMessage')

        return None

    def getresultlists(self, batchid):
        session = self.session
        url = "https://{instance}.salesforce.com/services/async/43.0/job/{jobid}/batch/{batchid}/result"
        url = url.format(instance=session.instance, jobid=self.jobid, batchid=batchid)
        headers = self.session.getheaders()

        response = requests.get(url=url, headers=headers)  # returns results list xml

        response_expl = response.text.split('<result>')
        result_list = []
        for i in range(1, len(response_expl)):
            result_elem = response_expl[i]
            result_list.append(result_elem.split('</result>')[0])

        self.batches[batchid]['results'] = result_list

    def getresults(self, batchid, resultid):
        if 'output' not in self.batches[batchid]:
            self.batches[batchid]['output'] = []

        session = self.session
        url = "https://{instance}.salesforce.com/services/async/43.0/job/{jobid}/batch/{batchid}/result/{resultid}"
        url = url.format(instance=session.instance, jobid=self.jobid, batchid=batchid, resultid=resultid)

        headers = self.session.getheaders()

        response = requests.get(url=url, headers=headers)
        self.batches[batchid]['output'].append(pandas.read_csv(io.StringIO(response.text)))
        return response.text

    def closeJob(self):
        session = self.session
        postdata = closeJobString
        url = "https://" + session.instance + ".salesforce.com/services/async/43.0/job/" + self.jobid

        headers = self.session.getheaders()

        response = requests.post(url=url, headers=headers, data=postdata)

        if getXMLData(response.text, 'state') == 'Closed':
            self.state = 'Closed'
            updatedb = "update jobs set state = '%s' where jobid = '%s'" % (self.state, self.jobid)
        return response.text

    def describe(self):
        description = "ID: {jobid}\nOperation: {operation}\nSalesforce Object: {sfobject}"
        return description.format(jobid=self.jobid, operation=self.operation, sfobject=self.object)

    def upload_data(self, data):
        self.addbatch(postdata=data)


class sfBatchApi:
    def __init__(self, session=sfSession()):
        self.session = session
        self.url = 'https://' + self.session.instance + '.salesforce.com'
        self.request_dict = {}

    def getheaders(self, content_type="application/xml; charset=UTF-8"):
        if self.session.isExpired():
            self.session.refreshToken()

        headers = {"Authorization": 'Bearer ' + self.session.sessionId,
                   "Content-Type": content_type}
        return headers

    def make_request(self):
        request_headers = self.getheaders()
        url = "https://" + self.session.instance + ".salesforce.com/services/data/v42.0/sobjects/"
        req = requests.get(url=url, headers=request_headers)
        return req

    def req(self, request_uri):
        # Check Cache
        request_headers = self.getheaders()

        response = requests.get(self.url + '/services/data/v42.0/' + request_uri
                                , headers=request_headers)
        self.request_dict[request_uri] = response.json()
        return response

    def object_information(self, object_name):
        request_headers = self.getheaders()
        url = "https://{instance_name}.salesforce.com/services/data/v42.0/sobjects/{object_name}".format( \
            instance_name=self.session.instance
            , object_name=object_name)
        req = requests.get(url, headers=request_headers)
        return req

    def object_fields(self, object_name):
        response = self.req(request_uri='sobjects/{object_name}/describe'.format(object_name=object_name))

        if response.status_code == 200:
            return response.json()
        else:
            return response.text

    def check_columns_exist(self, columns_to_check, field_list):
        columns_to_check = set([str(x).lower() for x in columns_to_check])
        field_list = set([str(x).lower() for x in field_list])
        if len(columns_to_check) == len(field_list.intersection(columns_to_check)):
            return True
        else:
            return False