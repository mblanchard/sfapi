import os

settingsdict = {"username": os.environ.get('SF_USERNAME'),
                "password": os.environ.get('SF_PASS'),  # Your SF Password
                "token": os.environ.get('SF_TOKEN'),  # Your SF security token
                "sqlitedirectory": ""
                # Enter a directory to store the logger file - best to just use the dir sfbulkapi is located,
                }
