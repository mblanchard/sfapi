from .auxiliary import sfSession, sfJob
import time, pandas


def detect_sObject(query):
    sObject = query.split(' from ')[1].split(' ')[0]
    return sObject


def soql_query(query, sObject=None, session=sfSession()):
    if not sObject:
        sObject = detect_sObject(query)

    job = sfJob('query', sObject, session)
    job.addbatch(query)
    job.closeJob()
    job.updatebatch()
    attemptnumber = 0
    print(job.batches)

    for key in job.batches:
        if job.batches[key]['state'] not in ('Queued', 'InProgress'):
            processing = False
            break
        else:
            processing = True
            break

    while processing == True:
        # All batches must finish to progress
        attemptnumber += 1
        time.sleep(10)
        processing = False
        print('Refresh number %d' % attemptnumber)
        job.updatebatch()
        print(job.batches)
        for key in job.batches:
            if job.batches[key]['state'] in ('Queued', 'InProgress'):
                processing = True

    for batchid in job.batches:
        job.getresultlists(batchid)

    assert job.batches.__len__() == 1, 'Cannot load two jobs into the same batch'

    for batchid in job.batches:
        for resultid in job.batches[batchid]['results']:
            job.getresults(batchid, resultid)

    df = pandas.concat(job.batches[list(job.batches.keys())[0]]['output'])

    return df


def runsoqltest():
    return soql_query("select id,name from user")
